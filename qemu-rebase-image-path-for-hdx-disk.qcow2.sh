########################################################################################################################
# qemu-rebase-image-path-for-hdx-disk.qcow2.sh
########################################################################################################################
# Copyright 2016 Jean-Christophe Manciot <manciot.jeanchristophe@gmail.com>
#
# Licensed under a Creative Commons Attribution 4.0 International License;
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://creativecommons.org/licenses/by/4.0/
#
# You are free to:
#
# o Share — copy and redistribute the material in any medium or format
# o Adapt — remix, transform, and build upon the material
# o for any purpose, even commercially.
#
# The licensor cannot revoke these freedoms as long as you follow the license terms.
#
# Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. 
# You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
########################################################################################################################

########################################################################################################################
#
# Changing the backing file for all qcow2 project files
#
########################################################################################################################
#
# Requirements:
# ------------
#
# - It must be launched at the top-level folder of the tree containing the qcow2 project files.
# - It uses the qemu-img utility contained in the qemu package.
# - I suggest to make a backup of all your files before launching the script in case you pass some wrong arguments.
#
########################################################################################################################
#
# Parameters:
# ----------
#
# - Name(s) of the qcow2 project files
#   For instance: *.qcow2
# - The new location of all images ***without a trailing /***
#   In GNS3, it matches "My binary images" in the "Edit -> Preferences -> General -> General" tab followed by /QEMU.
#
#######################################################################################################################
#
# Consequences:
# ------------
#
# - The path of the backing file for all qcow2 project files will be updated with the new location of the images.
# - The name of the image remains unchanged in the backing file.
# - A new <project_file>.info will be created for all project files to show the new backing file path.
#
########################################################################################################################
#
# Usage: example
# -----
#
# qemu-rebase-image-path-for-hdx-disk.qcow2.sh *.qcow2 /new/images/location/path/QEMU
#
########################################################################################################################
#!/bin/bash

# set -x

# Preventing globbing (pathname expansion)
# - wildcards
# - ranges enclosed in square brackets
# - non matching lists enclosed in square brackets
# - character classes (e.g. [[:xdigit:]])
set -o noglob

# If it contains globbing, we have to pass that 'argument' to the script to disable pattern globbing expansion for that parameter
files=$1

# The new location of all images ***without a trailing /*** - in GNS3, "images_path"
new_images_path=$2

source_folder=$(pwd)

# Array which will contain all found file names
file_list=()

while IFS= read -d $'\0' -r file ; do
	file_list=("${file_list[@]}" "$file")
done < <(find . -name "${files}" -print0)

echo ---------------------------------
echo "For each file in the list $files"
echo ---------------------------------
for file in "${file_list[@]}"
do
	# Going into the corresponding folder
	cd "${source_folder}/${file%/*}"

	# Extracting filename from file
	project_file=$(basename "$file")

        # Getting original "backing file"
        qemu-img info "$project_file" > "$project_file.info"

        # Extracting "backing file" path & name
        backing_path_filename=$(grep -o '^backing file: .*$' "$project_file.info" | cut -d ':' -f2)

        # Extracting "backing file" name
        backing_filename="${backing_path_filename##*/}"

        if [[ "$backing_filename" == "vios-adventerprisek9-m.vmdk" ]]; then
                backing_filename=$"IOSv-15.4.vmdk"
        fi

        # Rebasing the "backing file" with new_images_path
        qemu-img rebase -u -b "${new_images_path}/${backing_filename}" "$project_file"
        
        # Getting new "backing file"
        qemu-img info "$project_file" > "$project_file.info"

        # Printing the new "backing file"
        echo -e "\nNew backing file for ${source_folder}/${file%/*}/$project_file"
        qemu-img info "$project_file" | grep -o '^backing file: .*$'
done

cd "${source_folder}"

exit 0

